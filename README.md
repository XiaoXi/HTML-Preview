# GitHub & BitBucket HTML Preview

[![vercel](https://vercelbadge.soraharu.com/?app=html-preview)](https://htmlpreview.soraharu.com/)

许多 GitHub 存储库不使用 GitHub 页面托管其 HTML 文件。**GitHub & BitBucket HTML Preview** 允许您在不克隆或下载整个存储库的情况下呈现这些文件。它是一种客户端解决方案，使用 CORS 代理获取资源。

如果您尝试直接从 GitHub 在 Web 浏览器中打开任何 HTML、CSS 或 JS 文件的原始文件，您将只能看到源代码。GitHub 强迫他们使用 "text/plain" 内容类型，因此浏览器无法解析它们。此脚本使用 CORS 代理覆盖它。

## 🖥️ 使用

为了使用它，只需将此片段拼接到任何 HTML 文件的 URL 前：**[https://htmlpreview.soraharu.com/?](https://htmlpreview.soraharu.com/?)**

e.g.:

 - https://htmlpreview.soraharu.com/?https://github.com/user/repo/blob/master/index.html
 - https://htmlpreview.soraharu.com/?https://raw.githubusercontent.com/user/repo/master/index.html

它的作用是：使用 CORS 代理加载 HTML，然后处理所有链接、框架、脚本和样式，并使用 CORS 代理加载每个链接，以便浏览器可以对它们进行预览。

**GitHub & BitBucket HTML Preview** 在最新的 Google Chrome 和 Mozilla Firefox 下测试通过。

## 📜 开源许可

由 [niu tech](https://github.com/niutech) 开发 | 由 [XiaoXi](https://soraharu.com/) 本地化

基于 [Apache License 2.0](https://choosealicense.com/licenses/apache-2.0/) 许可进行开源。
